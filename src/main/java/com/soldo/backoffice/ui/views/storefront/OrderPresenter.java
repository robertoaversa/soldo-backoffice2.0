package com.soldo.backoffice.ui.views.storefront;

import java.util.List;
import java.util.stream.Collectors;

import com.soldo.backoffice.backend.data.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.flow.component.Focusable;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.soldo.backoffice.backend.data.entity.Order;
import com.soldo.backoffice.backend.data.entity.User;
import com.soldo.backoffice.backend.service.OrderService;
import com.soldo.backoffice.ui.crud.EntityPresenter;
import com.soldo.backoffice.ui.dataproviders.OrdersGridDataProvider;
import com.soldo.backoffice.ui.utils.BakeryConst;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OrderPresenter {

	private OrderCardHeaderGenerator headersGenerator;
	private StorefrontView view;

	private final EntityPresenter<Order> entityPresenter;
	private final OrdersGridDataProvider dataProvider;
	private final User currentUser;
	private final OrderService orderService;
	private boolean createNew = false;

	@Autowired
	OrderPresenter(OrderService orderService, OrdersGridDataProvider dataProvider,
			EntityPresenter<Order> entityPresenter)
				  // User currentUser)
	{
		this.orderService = orderService;
		this.entityPresenter = entityPresenter;
		this.dataProvider = dataProvider;
		this.currentUser = createUser("admin@vaadin.com", "admin", "admin", "peter", Role.ADMIN,
				"https://randomuser.me/api/portraits/men/10.jpg", false);
		headersGenerator = new OrderCardHeaderGenerator();
		headersGenerator.resetHeaderChain(false);
		dataProvider.setPageObserver(p -> headersGenerator.ordersRead(p.getContent()));
	}
	private User createUser(String email, String firstName, String lastName, String password, String role,
							String photoUrl, boolean locked) {
		User user = new User();
		user.setEmail(email);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setPassword(password);
		user.setRole(role);
		user.setPhotoUrl(photoUrl);
		user.setLocked(locked);
		return user;
	}

	void init(StorefrontView view) {
		this.entityPresenter.setView(view);
		this.view = view;
		view.getGrid().setDataProvider(dataProvider);
		view.getOpenedOrderEditor().setCurrentUser(currentUser);
		view.getOpenedOrderEditor().addCancelListener(e -> cancel());
		view.getOpenedOrderEditor().addReviewListener(e -> review());
		view.getOpenedOrderDetails().addSaveListenter(e -> save());
		view.getOpenedOrderDetails().addCancelListener(e -> cancel());
		view.getOpenedOrderDetails().addBackListener(e -> back());
		view.getOpenedOrderDetails().addEditListener(e -> edit());
		view.getOpenedOrderDetails().addCommentListener(e -> addComment(e.getMessage()));
	}

	OrderCardHeader getHeaderByOrderId(Long id) {
		return headersGenerator.get(id);
	}

	public void filterChanged(String filter, boolean showPrevious) {
		headersGenerator.resetHeaderChain(showPrevious);
		dataProvider.setFilter(new OrderFilter(filter, showPrevious));
	}

	void onNavigation(Long id, boolean edit) {
		createNew = false;
		entityPresenter.loadEntity(id, e -> open(e, edit));
	}

	void createNewOrder() {
		createNew = true;
		open(entityPresenter.createNew(), true);
	}
	
	void cancel() {
		entityPresenter.cancel(() -> close(false), () -> view.setOpened(true));
	}

	void edit() {
		UI.getCurrent().navigateTo(BakeryConst.PAGE_STOREFRONT_EDIT + "/" + entityPresenter.getEntity().getId());
	}

	void back() {
		view.setDialogElementsVisibility(true);
	}

	void review() {
		// Using collect instead of findFirst to assure all streams are
		// traversed, and every validation updates its view
		List<HasValue<?, ?>> fields = view.validate().collect(Collectors.toList());
		if (fields.isEmpty()) {
			if (entityPresenter.writeEntity()) {
				view.setDialogElementsVisibility(false);
				view.getOpenedOrderDetails().display(entityPresenter.getEntity(), true);
			}
		} else if (fields.get(0) instanceof Focusable) {
			((Focusable<?>) fields.get(0)).focus();
		}
	}

	void save() {
		entityPresenter.save(e -> close(true));
	}

	void addComment(String comment) {
		if (entityPresenter.executeUpdate(e -> orderService.addComment(currentUser, e, comment))) {
			// You can only add comments when in view mode, so reopening in that state.
			open(entityPresenter.getEntity(), false);
		}
	}

	private void open(Order order, boolean edit) {
		view.setDialogElementsVisibility(edit);
		view.setOpened(true);

		if (edit) {
			view.getOpenedOrderEditor().read(order);
		} else {
			view.getOpenedOrderDetails().display(order, false);
		}
	}

	private void close(boolean updated) {
		view.getOpenedOrderEditor().close();
		view.setOpened(false);


		if (createNew) {
			dataProvider.refreshAll();
		} else {
			view.navigateToMainView();
			if (updated) {
				dataProvider.refreshItem(entityPresenter.getEntity());
			}
		}
		entityPresenter.close();
	}
}
