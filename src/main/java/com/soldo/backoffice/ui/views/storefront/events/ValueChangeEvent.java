package com.soldo.backoffice.ui.views.storefront.events;

import com.vaadin.flow.component.ComponentEvent;
import com.soldo.backoffice.ui.views.storefront.OrderItemsEditor;

public class ValueChangeEvent extends ComponentEvent<OrderItemsEditor> {

	public ValueChangeEvent(OrderItemsEditor component) {
		super(component, false);
	}
}