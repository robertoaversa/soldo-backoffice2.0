package com.soldo.backoffice.ui.views.storefront;

import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.selection.SelectionEvent;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import com.vaadin.flow.templatemodel.TemplateModel;
import com.soldo.backoffice.app.HasLogger;
import com.soldo.backoffice.backend.data.entity.Order;
import com.soldo.backoffice.ui.MainView;
import com.soldo.backoffice.ui.components.SearchBar;
import com.soldo.backoffice.ui.utils.BakeryConst;
import com.soldo.backoffice.ui.views.EntityView;

@Tag("storefront-view")
@HtmlImport("src/views/storefront/storefront-view.html")
//@Route(value = BakeryConst.PAGE_STOREFRONT, layout = MainView.class)
//@RouteAlias(value = BakeryConst.PAGE_STOREFRONT_EDIT, layout = MainView.class)
//@RouteAlias(value = BakeryConst.PAGE_ROOT, layout = MainView.class)
//@PageTitle(BakeryConst.TITLE_STOREFRONT)
public class StorefrontView extends PolymerTemplate<TemplateModel>
		implements HasLogger, HasUrlParameter<Long>, EntityView<Order> {

	@Id("search")
	private SearchBar searchBar;

	@Id("grid")
	private Grid<Order> grid;

	@Id("dialog")
	private Dialog dialog;

	private final OrderEditor orderEditor;

	private final OrderDetails orderDetails = new OrderDetails();

	private final OrderPresenter presenter;

	@Autowired
	public StorefrontView(OrderPresenter presenter, OrderEditor orderEditor) {
		this.presenter = presenter;
		this.orderEditor = orderEditor;

		searchBar.setActionText("New order");
		searchBar.setCheckboxText("Show past orders");
		searchBar.setPlaceHolder("Search");

		dialog.add(orderDetails);

		grid.setSelectionMode(Grid.SelectionMode.SINGLE);
		grid.addColumn(new ComponentRenderer<>(order -> {
			OrderCard orderCard = new OrderCard();
			orderCard.setOrder(order);
			orderCard.setHeader(presenter.getHeaderByOrderId(order.getId()));
			orderCard.getElement().getClassList().add(BakeryConst.STOREFRONT_ORDER_CARD_STYLE);
			return orderCard;
		}));
		setOpened(false);
		grid.addSelectionListener(this::onOrdersGridSelectionChanged);
		getSearchBar().addFilterChangeListener(
				e -> presenter.filterChanged(getSearchBar().getFilter(), getSearchBar().isCheckboxChecked()));
		getSearchBar().addActionClickListener(e -> presenter.createNewOrder());

		presenter.init(this);

		dialog.getElement().addEventListener("opened-changed", e -> {
			if (!dialog.isOpened()) {
				// Handle client-side closing dialog on escape
				presenter.cancel();
			}
		});
	}

	void setOpened(boolean opened) {
		if (opened) {
			dialog.open();
		} else {
			dialog.close();
		}
	}

	@Override
	public void setParameter(BeforeEvent event, @OptionalParameter Long orderId) {
		boolean editView = event.getLocation().getPath().contains(BakeryConst.PAGE_STOREFRONT_EDIT);
		if (orderId != null) {
			presenter.onNavigation(orderId, editView);
		}
	}

	void navigateToMainView() {
		getUI().ifPresent(ui -> ui.navigateTo(BakeryConst.PAGE_STOREFRONT));
	}

	@Override
	public boolean isDirty() {
		return orderEditor.hasChanges();
	}

	@Override
	public void write(Order entity) throws ValidationException {
		orderEditor.write(entity);
	}

	public Stream<HasValue<?, ?>> validate() {
		return orderEditor.validate();
	}

	SearchBar getSearchBar() {
		return searchBar;
	}

	OrderEditor getOpenedOrderEditor() {
		return orderEditor;
	}

	OrderDetails getOpenedOrderDetails() {
		return orderDetails;
	}

	Grid<Order> getGrid() {
		return grid;
	}

	@Override
	public void clear() {
		orderEditor.clear();
	}

	private void onOrdersGridSelectionChanged(SelectionEvent<Order> e) {
		e.getFirstSelectedItem().ifPresent(order -> {
			getUI().ifPresent(ui -> ui.navigateTo(BakeryConst.PAGE_STOREFRONT + "/" + order.getId()));
			getGrid().deselect(order);
		});
	}

	void setDialogElementsVisibility(boolean editing) {
		if (editing) {
			dialog.add(orderEditor);
		}
		orderEditor.setVisible(editing);
		orderDetails.setVisible(!editing);
	}
}
