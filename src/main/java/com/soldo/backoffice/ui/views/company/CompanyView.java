package com.soldo.backoffice.ui.views.company;

import com.soldo.backoffice.app.HasLogger;
import com.soldo.backoffice.ui.MainView;
import com.soldo.backoffice.ui.components.ConfirmDialog;
import com.soldo.backoffice.ui.components.FormButtonsBar;
import com.soldo.backoffice.ui.utils.BakeryConst;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.Synchronize;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("company-view")
@HtmlImport("src/views/company/company-view.html")
@Route(value = BakeryConst.PAGE_COMPANY, layout = MainView.class)
@RouteAlias(value = BakeryConst.PAGE_ROOT, layout = MainView.class)
@PageTitle(BakeryConst.TITLE_COMPANY)
public class CompanyView extends PolymerTemplate<TemplateModel> {

public CompanyView() {
    System.out.println("Pipp");
}
}
