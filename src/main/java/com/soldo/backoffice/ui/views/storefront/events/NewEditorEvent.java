package com.soldo.backoffice.ui.views.storefront.events;

import com.vaadin.flow.component.ComponentEvent;
import com.soldo.backoffice.ui.views.storefront.OrderItemsEditor;

public class NewEditorEvent extends ComponentEvent<OrderItemsEditor> {

	public NewEditorEvent(OrderItemsEditor component) {
		super(component, false);
	}
}