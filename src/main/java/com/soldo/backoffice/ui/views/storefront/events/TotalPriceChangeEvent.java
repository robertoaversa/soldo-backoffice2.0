package com.soldo.backoffice.ui.views.storefront.events;

import com.vaadin.flow.component.ComponentEvent;
import com.soldo.backoffice.ui.views.storefront.OrderItemsEditor;

public class TotalPriceChangeEvent extends ComponentEvent<OrderItemsEditor> {

	private final Integer totalPrice;

	public TotalPriceChangeEvent(OrderItemsEditor component, Integer totalPrice) {
		super(component, false);
		this.totalPrice = totalPrice;
	}

	public Integer getTotalPrice() {
		return totalPrice;
	}

}