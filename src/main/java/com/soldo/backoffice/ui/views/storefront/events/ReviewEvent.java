package com.soldo.backoffice.ui.views.storefront.events;

import com.vaadin.flow.component.ComponentEvent;
import com.soldo.backoffice.ui.views.storefront.OrderEditor;

public class ReviewEvent extends ComponentEvent<OrderEditor> {

	public ReviewEvent(OrderEditor component) {
		super(component, false);
	}
}