package com.soldo.backoffice.ui.utils.converters;

import static com.soldo.backoffice.ui.dataproviders.DataProviderUtil.convertIfNotNull;
import static com.soldo.backoffice.ui.utils.FormattingUtils.FULL_DATE_FORMATTER;

import java.time.LocalDateTime;

import com.vaadin.flow.templatemodel.ModelConverter;

public class LocalDateTimeConverter implements ModelConverter<LocalDateTime, String> {


	private static final LocalTimeConverter TIME_FORMATTER = new LocalTimeConverter();

	@Override
	public String toPresentation(LocalDateTime modelValue) {
		return convertIfNotNull(modelValue,
				v -> FULL_DATE_FORMATTER.format(v) + " " + TIME_FORMATTER.toPresentation(v.toLocalTime()));
	}

	@Override
	public LocalDateTime toModel(String presentationValue) {
		throw new UnsupportedOperationException();
	}
}
