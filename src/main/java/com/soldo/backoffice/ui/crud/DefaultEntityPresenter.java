/**
 *
 */
package com.soldo.backoffice.ui.crud;

import com.vaadin.flow.data.provider.CallbackDataProvider;
import com.vaadin.flow.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.flow.data.provider.DataProvider;
import com.soldo.backoffice.backend.data.entity.AbstractEntity;
import com.soldo.backoffice.backend.service.FilterableCrudService;

public class DefaultEntityPresenter<T extends AbstractEntity> {

	private final ConfigurableFilterDataProvider<T, Void, String> filteredDataProvider;

	private final EntityPresenter<T> entityPresenter;

	private CrudView<T, ?> view;

	public DefaultEntityPresenter(EntityPresenter<T> entityPresenter, FilterableCrudService<T> crudService) {
		this.entityPresenter = entityPresenter;
		DataProvider<T, String> dataProvider = new CallbackDataProvider<>(
				query -> crudService.findAnyMatching(query.getFilter()).stream(),
				query -> crudService.findAnyMatching(query.getFilter()).size());

		filteredDataProvider = dataProvider.withConfigurableFilter();
	}

	public void init(CrudView<T, ?> view) {
		entityPresenter.setView(view);
		this.view = view;
		view.getGrid().setDataProvider(filteredDataProvider);
	}

	public void filter(String filter) {
		filteredDataProvider.setFilter(filter);
	}

	public void cancel() {
		entityPresenter.cancel(view::closeDialog, view::openDialog);
	}

	public void createNew() {
		open(entityPresenter.createNew());
	}

	public void loadEntity(Long id, boolean edit) {
		entityPresenter.loadEntity(id, this::open);
	}

	private void open(T entity) {
		view.getBinder().readBean(entity);
		view.getForm().getButtons().setSaveDisabled(true);
		view.getForm().getButtons().setDeleteDisabled(entity.isNew());
		view.updateTitle(entity.isNew());
		view.openDialog();
	}

	public void save() {
		if (entityPresenter.writeEntity()) {
			final boolean isNew = entityPresenter.getEntity().isNew();
			entityPresenter.save(e -> {
				if (isNew) {
					filteredDataProvider.refreshAll();
				} else {
					filteredDataProvider.refreshItem(e);
				}
				entityPresenter.close();
				view.closeDialog();
			});
		}
	}

	public void delete() {
		entityPresenter.delete(e -> {
			filteredDataProvider.refreshAll();
			entityPresenter.close();
			view.closeDialog();
		});
	}

	public void onValueChange(boolean isDirty) {
		view.getForm().getButtons().setSaveDisabled(!isDirty);
	}

}
