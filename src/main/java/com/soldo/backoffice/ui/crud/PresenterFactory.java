/**
 * 
 */
package com.soldo.backoffice.ui.crud;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.soldo.backoffice.backend.data.entity.Order;
import com.soldo.backoffice.backend.data.entity.Product;
import com.soldo.backoffice.backend.data.entity.User;
import com.soldo.backoffice.backend.service.OrderService;
import com.soldo.backoffice.backend.service.ProductService;
import com.soldo.backoffice.backend.service.UserService;

@Configuration
public class PresenterFactory {

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public DefaultEntityPresenter<Product> productPresenter(ProductService crudService)
		//		, User currentUser)
	{
		return new DefaultEntityPresenter<>(new EntityPresenter<>(crudService, null), crudService);
	}

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public DefaultEntityPresenter<User> userPresenter(UserService crudService) {
		return new DefaultEntityPresenter<>(new EntityPresenter<>(crudService, null), crudService);
	}

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public EntityPresenter<Order> orderEntityPresenter(OrderService crudService) {
		return new EntityPresenter<>(crudService, null);
	}

}
