package com.soldo.backoffice.backend.service;

import java.util.List;
import java.util.Optional;

import com.soldo.backoffice.backend.data.entity.AbstractEntity;

public interface FilterableCrudService<T extends AbstractEntity> extends CrudService<T> {

	List<T> findAnyMatching(Optional<String> filter);

}
