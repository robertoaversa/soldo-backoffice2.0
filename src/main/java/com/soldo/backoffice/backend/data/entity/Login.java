package com.soldo.backoffice.backend.data.entity;



import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "BO_LOGIN")
public class Login {

    private Long id;
    private String username;
    private String ip;
    private Date dateLogin;
    private Date dateLogout;

    public Login() {
    }

    public Login(String username, String ip, Date dateLogin, Date dateLogout) {
        this.username = username;
        this.ip = ip;
        this.dateLogin = dateLogin;
        this.dateLogout = dateLogout;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "ip")
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Column(name = "date_login")
    public Date getDateLogin() {
        return dateLogin;
    }

    public void setDateLogin(Date dateLogin) {
        this.dateLogin = dateLogin;
    }

    @Column(name = "date_logout")
    public Date getDateLogout() {
        return dateLogout;
    }

    public void setDateLogout(Date dateLogout) {
        this.dateLogout = dateLogout;
    }


}
