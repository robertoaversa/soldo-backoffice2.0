package com.soldo.backoffice.backend.data.entity;


import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "BO_USERS")
public class BackOfficeUser implements Serializable {
    public static final String BACKOFFICE_TYPE = "BO_USER";

    private String username;
    private String password;
    private String enabled;
    private String name;
    private String surname;
    private String officeLocation;
    private String jobPosition;
    private String notes;
    private Date dateCreation;
    private Date dateLastUpdate;
    private List<Authority> authorities = new ArrayList();
    public final static String TRUE = "TRUE";
    public final static String FALSE = "FALSE";
    private Date dateLastPassword;
    private String mustChangePassword;

    public BackOfficeUser() {
        authorities = new ArrayList<>();
    }

    @Id
    @Column(name = "username", unique = true, nullable = false)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "enabled")
    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String value) {
        this.enabled = value;
    }

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "username")
    public List<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<Authority> authorities) {
        this.authorities = authorities;
    }

    @Column(name = "date_creation")
    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    @Column(name = "date_last_update")
    public Date getDateLastUpdate() {
        return dateLastUpdate;
    }

    public void setDateLastUpdate(Date dateLastUpdate) {
        this.dateLastUpdate = dateLastUpdate;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    @Column(name = "surname")
    public String getSurname() {
        return surname;
    }

    @Column(name = "office_location")
    public String getOfficeLocation() {
        return officeLocation;
    }

    @Column(name = "job_position")
    public String getJobPosition() {
        return jobPosition;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setOfficeLocation(String officeLocation) {
        this.officeLocation = officeLocation;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition;
    }

    @Column(name = "date_last_password")
    public Date getDateLastPassword() {
        return dateLastPassword;
    }

    public void setDateLastPassword(Date dateLastPassword) {
        this.dateLastPassword = dateLastPassword;
    }

    @Column(name = "must_change_password")
    public String getMustChangePassword() {
        return mustChangePassword;
    }

    public void setMustChangePassword(String mustChangePassword) {
        this.mustChangePassword = mustChangePassword;
    }

    @Column(name= "notes")
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }


}