package com.soldo.backoffice.backend.data.entity;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "BO_AUTHORITIES")
public class Authority implements GrantedAuthority, Serializable {

    private String authority;
    private String username;
    private Long id;

    public Authority() {
    }

    public Authority(String role, String username) {
        this.authority = role;
        this.username = username;
    }

    @Column(name = "authority", nullable = true)
    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @Column(name = "username", nullable = true)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Authority)) return false;
        Authority that = (Authority) o;
        if (authority != null ? !authority.equals(that.getAuthority()) : that.getAuthority() != null) return false;
        return true;
    }

}