package com.soldo.backoffice.backend.repositories;

import com.soldo.backoffice.backend.data.entity.Login;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BackOfficeLoginRepository  extends JpaRepository<Login, Long> {
}
