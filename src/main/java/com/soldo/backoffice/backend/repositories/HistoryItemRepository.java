package com.soldo.backoffice.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.soldo.backoffice.backend.data.entity.HistoryItem;

public interface HistoryItemRepository extends JpaRepository<HistoryItem, Long> {
}
