package com.soldo.backoffice.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.soldo.backoffice.backend.data.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
