package com.soldo.backoffice.backend.repositories;

import com.soldo.backoffice.backend.data.entity.BackOfficeUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BackOfficeUserRepository extends JpaRepository<BackOfficeUser, Long> {

    //@Query("select * from BO_USERS user where user.username =:username")
    //BackOfficeUser findByUsername(@Param("username") String username);
    BackOfficeUser findByUsername(String username);

}
