package com.soldo.backoffice.app.security;



public class Role {

    public final static String ROLE_ADMIN = "ROLE_ADMIN";
    public final static String ROLE_CS_OPERATOR = "ROLE_CS_OPERATOR";
    public final static String ROLE_MARKETING = "ROLE_MARKETING";
    public final static String ROLE_FINANCE = "ROLE_FINANCE";
    public final static String ROLE_RISK_MANAGEMENT = "ROLE_RISK_MANAGEMENT";
    public final static String ROLE_PAYSAFE_AUDITOR = "ROLE_PAYSAFE_AUDITOR";

    public final static String ROLE_READONLY = "ROLE_READONLY";
    public final static String ROLE_USER = "ROLE_USER";

    public final static String ROLE_BUSINESS_MANAGEMENT = "ROLE_BUSINESS";
    public final static String ROLE_CONSUMER_MANAGEMENT = "ROLE_CONSUMER";
    public final static String ROLE_OPERATIONS = "ROLE_OPERATIONS";

    public static final String BARISTA = "barista";
    public static final String BAKER = "baker";
    // This role implicitly allows access to all views.
    public static final String ADMIN = "admin";

    public static String[] getRolesList() {
        return new String[] {ADMIN,BARISTA,BAKER,ROLE_ADMIN, ROLE_CS_OPERATOR, ROLE_MARKETING, ROLE_FINANCE, ROLE_RISK_MANAGEMENT, ROLE_PAYSAFE_AUDITOR, ROLE_BUSINESS_MANAGEMENT, ROLE_CONSUMER_MANAGEMENT, ROLE_OPERATIONS};
    }


}
