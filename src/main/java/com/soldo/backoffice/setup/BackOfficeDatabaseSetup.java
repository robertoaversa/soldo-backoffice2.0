package com.soldo.backoffice.setup;

import com.soldo.backoffice.backend.repositories.BackOfficeUserRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import soldo.core.context.BaseDatabaseSetup;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;


//"com.soldo.backoffice.model.repository"
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackageClasses = { BackOfficeUserRepository.class },
        entityManagerFactoryRef = "backOfficeEntityManagerFactory",
        transactionManagerRef = "backOfficeTransactionManager")
public class BackOfficeDatabaseSetup extends BaseDatabaseSetup {

    @Override
    protected String propertiesFile() {
        return "soldo/backoffice/dbms.properties";
    }

    @Override
    protected String jpaPropertiesFile() {
        return "soldo/backoffice/hibernate.properties";
    }

    @Override
    protected String[] packagesToScan() {
        return new String[]{"com.soldo.backoffice.backend.data.entity"};
    }

    @Bean(name = "backOfficeDataSource")
    @Override
    public DataSource dataSource() {
        return super.dataSource();
    }

    @Bean(name = "backOfficeEntityManagerFactory")
    @Override
    public EntityManagerFactory entityManagerFactory(@Qualifier("backOfficeDataSource") DataSource ds) {
        return super.entityManagerFactory(ds);
    }

    @Bean(name = "backOfficeTransactionManager")
    @Override
    public PlatformTransactionManager transactionManager(@Qualifier("backOfficeEntityManagerFactory") EntityManagerFactory emf) {
        return super.transactionManager(emf);
    }
}
