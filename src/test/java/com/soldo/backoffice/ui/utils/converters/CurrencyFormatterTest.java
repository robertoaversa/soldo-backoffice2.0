/**
 *
 */
package com.soldo.backoffice.ui.utils.converters;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.soldo.backoffice.test.FormattingTest;

public class CurrencyFormatterTest extends FormattingTest {

	@Test
	public void formattingShoudBeLocaleIndependent() {
		CurrencyFormatter formatter = new CurrencyFormatter();
		String result = formatter.toPresentation(123456);
		assertEquals("$1,234.56", result);
	}
}
