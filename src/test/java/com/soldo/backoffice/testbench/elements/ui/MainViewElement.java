package com.soldo.backoffice.testbench.elements.ui;

import com.soldo.backoffice.testbench.elements.components.BakeryNavigationElement;
import com.vaadin.testbench.TestBenchElement;
import com.vaadin.testbench.elementsbase.Element;

@Element("main-view")
public class MainViewElement extends TestBenchElement {

	public BakeryNavigationElement getMenu() {
		return $(BakeryNavigationElement.class).first();
	}

}
